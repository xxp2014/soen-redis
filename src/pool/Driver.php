<?php


namespace Soen\Redis\Pool;


use Soen\Pool\Connection;
use Soen\Pool\Pool;
use Soen\Redis\RedisInterface;

class Driver extends \Soen\Pool\Driver implements RedisInterface
{
    public $redis;
    public $config;

    public function __construct($config)
    {
         $this->config = $config;
    }

    /**
     * @return \Redis
     */
    public function create(){
        $redis = new \Redis();
        $host = $this->config['host'];
        $port = $this->config['port'];
        $timeout = $this->config['timeout'];
        $database = $this->config['database'];
        $password= $this->config['password'];
        $hasConn = $redis->connect($host, $port, $timeout);
        // 假设密码是字符串 0 也能通过这个校验
        if ('' != (string)$password) {
            $redis->auth($this->password);
        }
        $redis->select($database);
        $this->redis = $redis;
        return $this;
    }

    function __call($name, $arguments)
    {
        $result = call_user_func_array([$this->redis, $name], $arguments);
        return $result;
    }

}