<?php


namespace Soen\Redis\Pool;


use Soen\Pool\Pool;

class RedisPool extends Pool
{
    public function __construct($driver)
    {
        parent::__construct($driver);
    }
}