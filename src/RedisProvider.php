<?php


namespace Soen\Redis;


use Soen\Redis\Pool\Driver;
use Soen\Redis\Pool\RedisPool;

class RedisProvider
{
    protected $pool;

    public $config;
    

    public function __construct()
    {
        if(empty($this->config)){
            throw new \RuntimeException('redis配置异常!');
        }
        $driver = new Driver($this->config);
        $this->pool = new RedisPool($driver);
    }

	public function init(){
        $connection = $this->pool->reuse();
        \Swoole\Coroutine::defer(function ()use($connection){
            $this->pool->revert($connection);
        });
        $redisDriver = $connection->getConnection();
        $redisDriver->setPool($this->pool);
        $redisDriver->setConn($connection);
        return $redisDriver;
    }
    
}